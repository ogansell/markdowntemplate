---
output:
  html_document:
    theme: null
    highlight: null
    css: css/DataDriven3.css
    self_contained: yes
---
<!-- The following code adds the watercolour banner and is al the place to insert the title of the document -->

<div class="container-fluid">
<header>
<!-- Banner Image -->
<img id="img1" width="595px" height="185px" src="images/PlainBanner.png"/>
<!-- Add DOC logo -->
<img id="img2" src="images/DOC_logo.png" width = "180px" height = "64px">
<h1 align = "middle">Insert Main Title Here</h1>
</header>
</div>


<!-- Add some spaces between header and h1 subheader using the <br> tag -->
<br><br><br>

<h1 class="h1sub">
Reporting Template Secondary Title
</h1>

```{r setup, include=FALSE,echo=FALSE}
knitr::opts_chunk$set(echo = TRUE)
#setInternet2(TRUE)
require(devtools)
require(RCurl)
require(httr)
require(leaflet)
require(rgdal)
require(DT)
require(plotly)
require(sp)
require(knitr)
require(pander)

```

## Key findings

<p><b> Add some text in bold summarising the main key finding. Should just be a brief paragraph.Then a few bullet points that contain some more detail.</b></p>

* Main point 
* Another main point 
* And another



```{r DT table,include=TRUE,echo=FALSE,results='asis',warning=FALSE}

##2 options for tables: DT table using 'datatable' function or 'kable' using package 'knitr'
#DT is better for more complicated tables, for simple tables use kable

datatable(iris,style = "default",class = 'compact hover', filter = "none",selection = "none",escape = TRUE,caption="Table Caption",rownames = FALSE,extensions = 'Responsive')
#To change column names use:
#colnames = c('Name 1', 'Name 2')

#kable(iris, format = "html", caption = "Table caption")
#To change column names use:
#col.names = c('Name 1', 'Name 2')

```



```{r PCL leaflet,echo=FALSE,warning=FALSE}
#Make an interactive map if needed
m <- leaflet() %>%
  addProviderTiles("Esri.WorldImagery", group = "Satellite") %>%  # Add esri satellite imagery
  addProviderTiles("Esri.WorldTopoMap", group = "Topo") %>%
  setView(lng = 175.5, lat = -41.2, zoom = 6) %>% #set the view over NZ
#Add tile layer of PCL
  addTiles(
    "https://api.mapbox.com/styles/v1/ogansell/cioc1z34y005paem51cdvgxli/tiles/{z}/{x}/{y}?access_token=pk.eyJ1Ijoib2dhbnNlbGwiLCJhIjoiUW5oVUZnQSJ9.M49XpU8sGHvdtkU2E9s8Ww",
    attribution = "Koordinates Public Conservation Land",layerId="PCL",group = "PCL")%>%
#Add a legend  
  addLegend("bottomright", values = p,
            title = "",colors='#31770d',labels="Public Conservation Land",
            opacity = 1)%>%
#Add some controls to the map for the different layers. 'baseGroups' should be used for baselayers,'overlayGroups' for polygons, points etc you want to turn on or off  
  addLayersControl(baseGroups = c("Satellite","Topo"), options = layersControlOptions(collapsed = TRUE), overlayGroups = c("PCL"))

m  # Print the map

```


##Definition and methodology


*Define the information and note the source of the data*

Two paragraphs at most 

##Data quality

*Is it a case study or a national indicator. Meaning is inference limited or can it be extrapolated across an area of interest.*

This measures is classified as a [national indicator](http://www.stats.govt.nz/browse_for_stats/environment/environmental-reporting-series/environmental-indicators/Home/About.aspx#topics{:target="_blank"})

##Relevance

*This measures relates to indicator XX*

This measure relates to **a measure from the Outcome Monitoring Framework**

##Accuracy

*How accurate is the data.*

This measure complies with the <a href="http://www.stats.govt.nz/browse_for_stats/environment/environmental-reporting-series/environmental-indicators/Home/About.aspx#dataquality" target="_blank">data quality guidelines</a> used in New Zealand's Environmental Reporting framework.







